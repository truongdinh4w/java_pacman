package pacman.game;

import javax.swing.JFrame;
import java.awt.EventQueue;

public class Pacman extends JFrame{
	
	public Pacman() {
		initUI();
	}
	
	public void initUI() {
		add(new Board());
		setTitle("Pacman Game");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(540, 540);
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			Pacman ex = new Pacman();
			ex.setVisible(true);
		});

	}

}
