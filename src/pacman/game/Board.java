package pacman.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class Board extends JPanel implements ActionListener {
    private Dimension d;
    private Image ii;
    private final Font smallFont = new Font("Helvetica", Font.BOLD, 14);
    private Color dotColor = new Color(83, 169, 255); // couleur des pacgommes
    private Color mazeColor;

    //Game Status
    private boolean inGame = false;
    private boolean start = true;
    private boolean dying = false;
    private boolean losing = false;

    private final int BLOCK_SIZE = 30;
    private final int N_BLOCKS = 15;
    private final int SCREEN_SIZE = BLOCK_SIZE * N_BLOCKS;

    //Pacman Animation
    private final int PAC_ANIM_DELAY = 2;
    private final int PACMAN_ANIM_COUNT = 4;
    private final int MAX_FANTOMES = 6;
    private final int PACMAN_SPEED = 6;

    private int pacAnimCount = PAC_ANIM_DELAY;
    private int pacAnimDir = 1;
    private int pacmanAimPos = 0;

    //Fantomes
    private int N_FANTOMES = 4;
    private int pacsLeft, score;

    private int[] dx, dy;
    private int[] fant_x, fant_y, fant_dy, fant_dx, fantSpeed;

    private int pacman_x, pacman_y, pacman_dx, pacman_dy;
    private int dir_dx, dir_dy, req_dx, req_dy;

    private final short levelData[] = {
    		19, 26, 26, 18, 26, 26, 22, 00, 19, 26, 26, 18, 26, 26, 22,
    		21, 00, 00, 21, 00, 00, 21, 00, 21, 00, 00, 21, 00, 00, 21,
    		17, 18, 26, 24, 26, 18, 24, 26, 24, 18, 26, 16, 26, 26, 20,
    		17, 20, 00, 00, 00, 21, 00, 00, 00, 21, 00, 21, 00, 00, 21,
    		25, 24, 26, 22, 00, 17, 22, 00, 19, 20, 00, 17, 26, 26, 28,
    		00, 00, 00, 17, 18, 00, 24, 26, 24, 16, 18, 20, 00, 00, 00,
    		00, 00, 00, 17, 00, 20, 00, 00, 00, 17, 00, 20, 00, 00, 00,
    		19, 18, 18, 00, 00, 20, 00, 00, 00, 17, 00, 00, 18, 18, 22,
    		25, 24, 24, 16, 16, 16, 18, 18, 18, 00, 16, 00, 24, 24, 28,
    		00, 00, 00, 17, 24, 00, 00, 24, 16, 16, 24, 20, 00, 00, 00,
    		19, 26, 26, 20, 00, 17, 28, 00, 9, 20, 00, 9, 26, 18, 22,
    		21, 00, 00, 21, 00, 21, 00, 00, 00, 21, 00, 00, 00, 17, 20,
    		17, 26, 26, 00, 26, 24, 18, 26, 18, 24, 26, 18, 26, 24, 20,
    		21, 00, 00, 21, 00, 00, 21, 00, 21, 00, 00, 21, 00, 00, 21,
    		25, 26, 26, 24, 26, 26, 28, 00, 25, 26, 26, 24, 26, 26, 28
    };

    private final int maxSpeed = 6;
    private int currentSpeed = 3;

    private short[] screenData;
    private Timer timer;

    public Board() {
        initVariables();
        initBoard();
    }


    private void initBoard() {
        setFocusable(true);
        setBackground(Color.black);
        addKeyListener(new TAdapter());
    }

    private void initVariables() {
        screenData = new short[N_BLOCKS * N_BLOCKS];
        mazeColor = new Color(0, 255, 0);
        d = new Dimension(400, 400);
        fant_x = new int[MAX_FANTOMES];
        fant_y = new int[MAX_FANTOMES];
        fant_dx = new int[MAX_FANTOMES];
        fant_dy = new int[MAX_FANTOMES];
        fantSpeed = new int[MAX_FANTOMES];
        dx = new int[4];
        dy = new int[4];

        timer = new Timer(40, this);
        timer.start();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        initGame();
    }

    private void playGame(Graphics2D g2d) {
    	
    	if(dying == true) {
    		death();
    	}else {
    	
        pacmanMove();
        fantomesMove(g2d);
        drawPacman(g2d);
        //Kiem tra xem win game
        checkMaze();
        
        }
    }
    

    
    private void checkMaze() {
		
		int i = 0;
		boolean endGame = true;
		
		
		while ( i < N_BLOCKS*N_BLOCKS && endGame) {
			if((screenData[i] & 48) != 0) {
				endGame = false;
			}
			i++;
		}
		if(endGame) {
			if(N_FANTOMES < MAX_FANTOMES) {
				N_FANTOMES ++;
			}
			if(currentSpeed < maxSpeed) {
				currentSpeed++;
			}
			
			initLevel();
		}
	}

	private void death() {
		pacsLeft--;
		
		
		if(pacsLeft == 0) {
			inGame = false;
			losing = true;
		}
		continueLevel();
		
	}
	private void drawScore(Graphics2D g2d) {
		
		String s, p;
		
		g2d.setFont(smallFont);
		g2d.setColor(Color.white);
		s = "Score: " + getScore();
		
		
		p = " Lives: " + pacsLeft;
		g2d.drawString(s, SCREEN_SIZE /2, SCREEN_SIZE + 20);
		g2d.drawString(p, 80, SCREEN_SIZE + 20);
		g2d.setColor(Color.yellow); //backgroud
        g2d.fillOval(16, SCREEN_SIZE +2 , 25, 25);
		
		
	}
	void setScore(int score)
	{
		this.score = score;
	}
	int getScore()
	{
		return score;
	}

	private void fantomesMove(Graphics2D g2d) {
		short i;
		int pos;
		int count;
		
		for (i =0; i < N_FANTOMES; i++) {
			if(fant_x[i] % BLOCK_SIZE == 0 && fant_y[i] % BLOCK_SIZE == 0) {
				pos = fant_x[i]/ BLOCK_SIZE + N_BLOCKS*(int)(fant_y[i]/ BLOCK_SIZE);
				
				count = 0;
				// Neu khong phai tuong ben trai va fant duoc di chuyen sang ben trai
				if((screenData[pos] & 1) == 0 && fant_dx[i] != 1) {
					dx[count] = -1;
					dy[count] = 0;
					count++;
					// System.out.println(screenData[pos]);
				}
				
				if((screenData[pos] & 2) == 0 && fant_dy[i] != 1) {
					dx[count] = 0;
					dy[count] = -1;
					count++;
				}
				
				if((screenData[pos] & 4) == 0 && fant_dx[i] != -1) {
					dx[count] = 1;
					dy[count] = 0;
					count++;
				}
				
				if((screenData[pos] & 8) == 0 && fant_dy[i] != -1) {
					dx[count] = 0;
					dy[count] = 1;
					count++;
				}
				
				if(count == 0) {
					if((screenData[pos] & 15) == 15) {
						fant_dx[i] = 0;
						fant_dy[i] = 0;
					}else {
						fant_dx[i] = - fant_dx[i];
						fant_dy[i] = - fant_dy[i];
					}
				}else {
					count = (int)(Math.random()*count);
					
					if(count > 3) {
						count = 3;
					}
					
					fant_dx[i] = dx[count];
					fant_dy[i] = dy[count];
				}
				
			}
			
			fant_x[i] = fant_x[i] + (fant_dx[i]*fantSpeed[i]);
			fant_y[i] = fant_y[i] + (fant_dy[i]*fantSpeed[i]);
			drawFantome(g2d, fant_x[i] + 1, fant_y[i] + 1);
			
			if(pacman_x > (fant_x[i] - (BLOCK_SIZE/2)) && pacman_x < (fant_x[i] + (BLOCK_SIZE/2))
					&& pacman_y > (fant_y[i] - (BLOCK_SIZE/2)) && pacman_y < (fant_y[i] + (BLOCK_SIZE/2))
					&& inGame) {
				dying = true;
			}
		}
		
	}
	
  private void startScreenGame(Graphics2D g) {
	g.setColor(new Color(0,32,48));
	g.fillRect(50, SCREEN_SIZE/2 -30, SCREEN_SIZE - 100, 50);
	g.setColor(Color.white);
	g.drawRect(50, SCREEN_SIZE/2 -30, SCREEN_SIZE - 100, 50);
	
	String s = "Press s to start.";
	Font small = new Font("Helvetica", Font.BOLD, 14);
	FontMetrics metr = this.getFontMetrics(small);

	g.setColor(Color.white);
	g.setFont(small);
	g.drawString(s, (SCREEN_SIZE - metr.stringWidth(s)) / 2, SCREEN_SIZE / 2);
}

	
	private void drawLosing(Graphics2D g2d) {
		g2d.setColor(new Color(0,32,48));
    	g2d.fillRect(50, SCREEN_SIZE/2 -30, SCREEN_SIZE - 100, 50);
    	g2d.setColor(Color.white);
    	g2d.drawRect(50, SCREEN_SIZE/2 -30, SCREEN_SIZE - 100, 50);
    	
    	String s = "You lose! Press s to start.";
    	Font small = new Font("Helvetica", Font.BOLD, 14);
    	FontMetrics metr = this.getFontMetrics(small);

    	g2d.setColor(Color.white);
    	g2d.setFont(small);
    	g2d.drawString(s, (SCREEN_SIZE - metr.stringWidth(s)) / 2, SCREEN_SIZE / 2);
	}
    
    // Draw Fantomes
    
    private void drawFantome(Graphics2D g2d, int x, int y) {
    	g2d.setColor(new Color(249, 244, 223)); //backgroud
        g2d.fillOval(x, y, 15, 25);
    }

	private void doAnimation() {
    	pacAnimCount--;
    	
    	if(pacAnimCount <= 0) {
    		pacAnimCount = PAC_ANIM_DELAY;
    		
    		pacmanAimPos = pacmanAimPos + pacAnimDir; //2 ,3 2,1,0
    		if(pacmanAimPos == (PACMAN_ANIM_COUNT - 1)|| pacmanAimPos == 0) {
    			pacAnimDir = - pacAnimDir;
    		}
    	}
  		
  	}

    //Move of pacman
    private void pacmanMove() {
        int pos;
        int hit;

        if (req_dx == -pacman_dx && req_dy == pacman_dy) {
            pacman_dx = req_dx;
            pacman_dy = req_dy;

            dir_dx = pacman_dx;
            dir_dy = pacman_dy;
        }
        if (pacman_x % BLOCK_SIZE == 0 && pacman_y % BLOCK_SIZE == 0) {
            pos = pacman_x / BLOCK_SIZE + N_BLOCKS * (int) (pacman_y / BLOCK_SIZE);
            hit = screenData[pos];

            if ((hit & 16) != 0) {
                screenData[pos] = (short) (hit & 15); // supprimer gom au pos
                score = score + 100;
            }
            if(this.score % 5000 == 0 && this.score >0) {
        		this.pacsLeft++;
        		
        	}

            //Pacman se déplace quand dx et dy  # 0
            if ((req_dx != 0) || (req_dy != 0)) {
                if (!((req_dx == -1 && req_dy == 0 && (hit & 1) != 0) ||
                        (req_dx == 1 && req_dy == 0 && (hit & 4) != 0) ||
                        (req_dx == 0 && req_dy == -1 && (hit & 2) != 0) ||
                        (req_dx == 0 && req_dy == 1 && (hit & 8) != 0))
                        ) { // tourne à gauche
                    pacman_dx = req_dx;
                    pacman_dy = req_dy;
                    dir_dx = pacman_dx;
                    dir_dy = pacman_dy;
                }
            }

            if ((pacman_dx == -1 && pacman_dy == 0 && (hit & 1) != 0) ||
                    (pacman_dx == 1 && pacman_dy == 0 && (hit & 4) != 0) ||
                    (pacman_dx == 0 && pacman_dy == -1 && (hit & 2) != 0) ||
                    (pacman_dx == 0 && pacman_dy == 1 && (hit & 8) != 0)) {
                pacman_dx = 0;
                pacman_dy = 0;
            }
        }
        pacman_x = pacman_x + PACMAN_SPEED * pacman_dx;
        pacman_y = pacman_y + PACMAN_SPEED * pacman_dy;
        
    }

 

    private void drawPacman(Graphics2D g2d) {
    	g2d.setColor(Color.yellow); //backgroud
        g2d.fillOval(pacman_x +1, pacman_y +1 , 25, 25);
    }

    private void initGame() {
        pacsLeft = 3;
        score = 0;
        initLevel();
        N_FANTOMES = 4;
        currentSpeed = 3;
       
    }

    private void initLevel() {
        int i;
        for (i = 0; i < N_BLOCKS * N_BLOCKS; i++) {
            screenData[i] = levelData[i];
        }
        
        continueLevel();
        
    }

    private void continueLevel() {
		
    	int i;
    	int dx = 1;
    	for(i=0; i<N_FANTOMES; i++) {
    		fant_y[i] = 7*BLOCK_SIZE; 
    		fant_x[i] = 7*BLOCK_SIZE;
    		fant_dx[i] = dx;
    		fant_dy[i] = 0;
            dx = -dx;

            fantSpeed[i] = 2;
        }

        pacman_x = 7 * BLOCK_SIZE;
        pacman_y = 11 * BLOCK_SIZE;
        pacman_dx = 0;
        pacman_dy = 0;
        req_dx = 0;
        req_dy = 0;
        dir_dx = -1;
        dir_dy = 0;
        dying = false;
        
    }

	private void drawMaze(Graphics2D g2d) {
        int i = 0;
        int x, y;
        for (y = 0; y < SCREEN_SIZE; y += BLOCK_SIZE) {
            for (x = 0; x < SCREEN_SIZE; x += BLOCK_SIZE) {
                g2d.setColor(mazeColor);
                g2d.setStroke(new BasicStroke(2));

                if ((screenData[i] & 1) != 0) {
                    g2d.drawLine(x, y, x, y + BLOCK_SIZE - 1);
                   
                }
                if ((screenData[i] & 2) != 0) {
                    g2d.drawLine(x, y, x + BLOCK_SIZE - 1, y);
                    
                }
                if ((screenData[i] & 4) != 0) {
                    g2d.drawLine(x + BLOCK_SIZE - 1, y, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }
                if ((screenData[i] & 8) != 0) {
                    g2d.drawLine(x, y + BLOCK_SIZE - 1, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }
                if ((screenData[i] & 16) != 0) {
                    g2d.setColor(dotColor);
                    g2d.fillOval(x + (BLOCK_SIZE / 2), y + (BLOCK_SIZE / 2), 5, 5);
                }
                i++;
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Drawing(g);
    }

    private void Drawing(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.black); //backgroud
        g2d.fillRect(0, 0, d.width, d.height);
        doAnimation();
        drawMaze(g2d);
        
        drawScore(g2d);
        
        if(inGame) {
        	playGame(g2d);
        } else {
        	if(start) {
        	startScreenGame(g2d);
        	} else {
        		drawLosing(g2d);
        	}
        }
        
        
        Toolkit.getDefaultToolkit().sync();
        g2d.dispose();
    }

    

	class TAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if (inGame) {
                if (key == KeyEvent.VK_LEFT) {
                    req_dx = -1;
                    req_dy = 0;
                } else if (key == KeyEvent.VK_RIGHT) {
                    req_dx = 1;
                    req_dy = 0;
                } else if (key == KeyEvent.VK_UP) {
                    req_dx = 0;
                    req_dy = -1;
                } else {
                    req_dx = 0;
                    req_dy = 1;
                }
            }else {
            	if(key == 's' || key == 'S') {
            		start = false;
            		inGame = true;
            		initGame();
            	}
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            if (key == Event.LEFT || key == Event.RIGHT || key == Event.UP
                    || key == Event.DOWN) {
                req_dx = 0;
                req_dy = 0;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
}
